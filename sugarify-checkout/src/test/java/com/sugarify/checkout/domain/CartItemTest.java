/**
 * 
 */
package com.sugarify.checkout.domain;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

import org.testng.annotations.Test;

import com.sugarify.checkout.CheckoutException;

/**
 * @author ganeshs
 *
 */
public class CartItemTest {

	@Test
	public void shouldUpdateQuantity() {
		CartItem item = new CartItem();
		item.updateQuantity(10);
		assertEquals(item.getQuantity(), 10);
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotUpdateZeroOrNegativeQuantity() {
		CartItem item = new CartItem();
		item.updateQuantity(-1);
	}
	
	@Test
	public void shouldMergeItemQuantiy() {
		CartItem item1 = new CartItem();
		item1.setQuantity(2);
		CartItem item2 = new CartItem();
		item2.setQuantity(1);
		item1.merge(item2);
		assertEquals(item1.getQuantity(), 3);
	}
	
	@Test
	public void shouldUnmarkItemAsDeletedDuringMerge() {
		CartItem item1 = new CartItem();
		item1.setDeleted(true);
		item1.setQuantity(2);
		CartItem item2 = new CartItem();
		item2.setQuantity(1);
		item1.merge(item2);
		assertEquals(item1.getQuantity(), 1);
		assertFalse(item1.getDeleted());
	}
}

/**
 * 
 */
package com.sugarify.checkout.domain;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.sql.Timestamp;

import org.activejpa.entity.testng.BaseModelTest;
import org.activejpa.jpa.JPA;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.Sets;
import com.sugarify.checkout.CheckoutException;
import com.sugarify.checkout.domain.Cart.Status;


/**
 * @author ganeshs
 *
 */
public class CartTest extends BaseModelTest {
	
	private Cart cart;
	
	private CartItem cartItem;

	@BeforeClass
	public void beforeClass() {
		JPA.instance.addPersistenceUnit("test");
	}

	public void setup() throws Exception {
		cart = new Cart();
		cartItem = new CartItem(1L, 1L, 2);
		cartItem.setCart(cart);
		cart.setItems(Sets.newHashSet(cartItem));
		cart.persist();
		cart = Cart.findById(cart.getId());
	}
	
	public void destroy() {
	}
	
	@Test
	public void shouldValidateCart() {
		cart.setStatus(Status.active);
		cart.validate();
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldThrowExceptionIfCartIsNotInActiveState() {
		cart.setStatus(Status.pending);
		cart.validate();
	}
	
	@Test
	public void shouldLockCart() {
		cart.lock();
		assertTrue(cart.isLocked());
		assertTrue(cart.getLockExpiryTime().after(new Timestamp(System.currentTimeMillis())));
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotLockCartIfLockNotAvailable() {
		cart.lock();
		cart.lock();
	}
	
	@Test
	public void shouldReleaseCart() {
		cart.lock();
		cart.release();
		assertFalse(cart.isLocked());
	}
	
	@Test
	public void shouldAddItemIfLockIsAvailable() {
		CartItem item = new CartItem(2L, 1L, 2);
		cart.addItem(item);
		assertTrue(cart.getItems().contains(item));
		assertFalse(cart.isLocked());
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotAddItemIfLockIsUnAvailable() {
		cart.lock();
		cart.addItem(new CartItem(2L, 1L, 2));
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotAddItemIfCartIsInvalid() {
		cart.setStatus(Status.pending);
		cart.addItem(new CartItem(2L, 1L, 2));
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotRemoveItemIfLockIsUnAvailable() {
		cart.lock();
		cart.removeItem(cartItem);
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotRemoveItemIfCartIsInvalid() {
		cart.setStatus(Status.pending);
		cart.removeItem(cartItem);
	}
	
	@Test
	public void shouldUpdateQuantityIfLockIsAvailable() {
		cart.updateItemQuantity(cartItem, 4);
		assertEquals(cartItem.getQuantity(), 4);
		assertFalse(cart.isLocked());
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotUpdateQuantityIfLockIsNotAvailable() {
		cart.lock();
		cart.updateItemQuantity(cartItem, 4);
	}
	
	@Test(expectedExceptions=CheckoutException.class)
	public void shouldNotUpdateQuantityIfCartIsInvalid() {
		cart.setStatus(Status.pending);
		cart.updateItemQuantity(cartItem, 4);
	}
	
	@Test
	public void shouldMergeItemIfAlreadyExist() {
		CartItem item = new CartItem(cartItem.getMerchantProductId(), cartItem.getVariantId(), 10);
		cart.addItem(item);
		assertEquals(cart.getItems().size(), 1);
		assertEquals(cartItem.getQuantity(), 12);
	}
}

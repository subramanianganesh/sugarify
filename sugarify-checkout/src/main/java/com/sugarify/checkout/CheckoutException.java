/**
 * 
 */
package com.sugarify.checkout;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.server.exception.ApplicationException;

/**
 * @author ganeshs
 *
 */
public class CheckoutException extends ApplicationException {
	
	private static final long serialVersionUID = 1L;

	/**
	 * @author ganeshs
	 *
	 */
	public enum ErrorCode {
		
		lock_not_available(HttpResponseStatus.CONFLICT), invalid_item_quantity(HttpResponseStatus.UNPROCESSABLE_ENTITY), cart_not_active(HttpResponseStatus.UNPROCESSABLE_ENTITY);
		
		private HttpResponseStatus status;
		
		/**
		 * @param status
		 */
		private ErrorCode(HttpResponseStatus status) {
			this.status = status;
		}
	}

	/**
	 * @param code
	 * @param message
	 */
	public CheckoutException(ErrorCode code, String message) {
		super(code.status, message);
	}
	
	/**
	 * @param code
	 * @param message
	 * @param cause
	 */
	public CheckoutException(ErrorCode code, String message, Throwable cause) {
		super(code.status, message, cause);
	}

}

/**
 * 
 */
package com.sugarify.checkout.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sugarify.checkout.CheckoutException;
import com.sugarify.checkout.CheckoutException.ErrorCode;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="cart_items")
@Access(AccessType.FIELD)
public class CartItem extends BaseRecordableDomain {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cartId")
	@JsonBackReference
	private Cart cart;
	
	@Min(value=1)
	private int quantity;
	
	@NotNull
	private Long merchantProductId;
	
	@NotNull
	private Long variantId;
	
	private Boolean deleted = false;
	
	/**
	 * Default constructor
	 */
	public CartItem() {
	}

	/**
	 * @param merchantProductId
	 * @param variantId
	 * @param quantity
	 */
	public CartItem(Long merchantProductId, Long variantId, int quantity) {
		this.quantity = quantity;
		this.merchantProductId = merchantProductId;
		this.variantId = variantId;
	}

	/**
	 * @return the cart
	 */
	public Cart getCart() {
		return cart;
	}

	/**
	 * @param cart the cart to set
	 */
	public void setCart(Cart cart) {
		this.cart = cart;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the variantId
	 */
	public Long getVariantId() {
		return variantId;
	}

	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(Long variantId) {
		this.variantId = variantId;
	}

	/**
	 * @return the deleted
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the merchantProductId
	 */
	public Long getMerchantProductId() {
		return merchantProductId;
	}

	/**
	 * @param merchantProductId the merchantProductId to set
	 */
	public void setMerchantProductId(Long merchantProductId) {
		this.merchantProductId = merchantProductId;
	}
	
	/**
	 * Updates the quantity
	 * 
	 * @param quantity
	 */
	public void updateQuantity(int quantity) {
		if (quantity <= 0) {
			 throw new CheckoutException(ErrorCode.invalid_item_quantity, "Invalid quantity specified for the cart item - " + getId()); 
		}
		setQuantity(quantity);
	}
	
	/**
	 * Merge the given item with this item. 
	 * 
	 * <p>If the current item is in deleted state, it marks it as undeleted and sets the quantity to new quantity else adds the new quantity to the current item.
	 * 
	 * @param item
	 */
	public void merge(CartItem item) {
		if (getDeleted()) {
			setDeleted(false);
			updateQuantity(item.getQuantity());
		} else {
			updateQuantity(item.getQuantity() + getQuantity());
		}
	}
}

/**
 * 
 */
package com.sugarify.checkout.domain;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.LockModeType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.activejpa.entity.EntityCollection;
import org.apache.commons.lang3.time.DateUtils;
import org.minnal.instrument.entity.Action;
import org.minnal.instrument.entity.AggregateRoot;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sugarify.checkout.CheckoutException;
import com.sugarify.checkout.CheckoutException.ErrorCode;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="carts")
@AggregateRoot
@Access(AccessType.FIELD)
public class Cart extends BaseRecordableDomain {
	
	public enum Status {
		active, pending, completed, expired
	}

	private Long userId;
	
	private boolean deleted;
	
	@OneToMany(mappedBy="cart", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference
	private Set<CartItem> items;
	
	@Enumerated(EnumType.STRING)
	private Status status = Status.active;
	
	private boolean locked;
	
	private Timestamp lockExpiryTime;
	
	public static final Integer LOCK_EXPIRY_INTERVAL = 300; 

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the items
	 */
	public Set<CartItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Set<CartItem> items) {
		this.items = items;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * @param locked the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	/**
	 * @return the lockExpiryTime
	 */
	public Timestamp getLockExpiryTime() {
		return lockExpiryTime;
	}

	/**
	 * @param lockExpiryTime the lockExpiryTime to set
	 */
	public void setLockExpiryTime(Timestamp lockExpiryTime) {
		this.lockExpiryTime = lockExpiryTime;
	}
	
	/**
	 * @param item
	 */
	public void addItem(CartItem item) {
		validate();
		lock();
		try {
			EntityCollection<CartItem> collection = collection("items");
			CartItem existingItem = collection.first("items.merchantProductId", item.getMerchantProductId(), "items.variantId", item.getVariantId(), "items.deleted", false);
			if (existingItem != null) {
				existingItem.merge(item);
			} else {
				item.setCart(this);
				this.items.add(item);
			}
		} finally {
			release();
		}
	}
	
	/**
	 * @param item
	 */
	public void removeItem(CartItem item) {
		validate();
		lock();
		try {
			this.items.remove(item);
		} finally {
			release();
		}
	}
	
	/**
	 * Locks the cart
	 */
	public void lock() {
		if (! execute(new Executor<Boolean>() {
			public Boolean execute(EntityManager manager) {
				manager.lock(Cart.this, LockModeType.PESSIMISTIC_WRITE);
				refresh();
				Timestamp currentTime = new Timestamp(System.currentTimeMillis());
				Timestamp expiryTime = new Timestamp(DateUtils.addSeconds(currentTime, LOCK_EXPIRY_INTERVAL).getTime());
				if (isLocked() && lockExpiryTime != null && lockExpiryTime.after(currentTime)) {
					return false;
				}
				setLocked(true);
				setLockExpiryTime(expiryTime);
				persist();
				return true;
			}
		}, false)) {
			throw new CheckoutException(ErrorCode.lock_not_available, "Cart - " + getId() + " is already being processed");
		}
	}
	
	/**
	 * Releases the lock on the cart
	 */
	public void release() {
		setLocked(false);
		persist();
	}
	
	/**
	 * Update the cart item quantity
	 * 
	 * @param item
	 * @param quantity
	 */
	@Action(value="updateQuantity", path="items")
	public void updateItemQuantity(CartItem item, int quantity) {
		validate();
		lock();
		try {
			item.updateQuantity(quantity);
			persist();
		} finally {
			release();
		}
	}
	
	/**
	 * Validates the cart
	 */
	public void validate() {
		if (status != Status.active) {
			throw new CheckoutException(ErrorCode.cart_not_active, "Cart " + getId() + " is not active");
		}
	}
}



/**
 * 
 */
package com.sugarify.core.domain;

import java.sql.Timestamp;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import org.minnal.jpa.entity.BaseDomain;

/**
 * @author ganeshs
 *
 */
@MappedSuperclass
public class BaseRecordableDomain extends BaseDomain {

	private Timestamp createdAt;
	
	private Timestamp updatedAt;

	/**
	 * @return the createdAt
	 */
	public Timestamp getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	/**
	 * Handles pre-persist operations
	 */
	@PrePersist
	protected void prePersist() {
		setCreatedAt(new Timestamp(System.currentTimeMillis()));
	}
}

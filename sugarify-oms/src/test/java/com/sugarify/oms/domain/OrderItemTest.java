/**
 * 
 */
package com.sugarify.oms.domain;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import java.sql.Timestamp;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.sugarify.oms.OMSException;
import com.sugarify.oms.domain.OrderItem.Status;
import com.sugarify.oms.domain.OrderItemAdjustment.Type;

/**
 * @author ganeshs
 *
 */
public class OrderItemTest {
	
	private OrderItem item;
	
	@BeforeMethod
	public void setup() {
		item = spy(new OrderItem(1L, 2L, 3L, 2, "Test Item", "Test Description", 10d, new Timestamp(System.currentTimeMillis())));
	}
	
	@Test
	public void shouldCheckIfItemCostCanBeComputed() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.TOTAL_COST_COMPUTABLE_STATES.contains(status)) {
				assertTrue(item.canComputeTotalCost());
			} else {
				assertFalse(item.canComputeTotalCost());
			}
		}
	}

	@Test
	public void shouldCheckIfItemCanBeApproved() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.APPROVABLE_STATES.contains(status)) {
				assertTrue(item.canApprove());
			} else {
				assertFalse(item.canApprove());
			}
		}
	}
	
	@Test
	public void shouldCheckIfItemCanBeConfirmed() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (status == Status.approved) {
				assertTrue(item.canConfirm());
			} else {
				assertFalse(item.canConfirm());
			}
		}
	}
	
	@Test
	public void shouldCheckIfItemCanBeCancelled() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.CANCELLABLE_STATES.contains(status)) {
				assertTrue(item.canCancel(item.getQuantity()));
			} else {
				assertFalse(item.canCancel(item.getQuantity()));
			}
		}
	}
	
	@Test
	public void shouldCheckExceptionThrownIfItemCantBeCancelled() {
		for (Status status : Status.values()) {
			item.setStatus(status);;
			if (OrderItem.CANCELLABLE_STATES.contains(status)) {
				assertTrue(item.canCancel(item.getQuantity(), true));
			} else {
				try {
					item.canCancel(item.getQuantity(), true);
					fail("Expected OMSException for the status " + status);
				} catch (OMSException e) {
				}
			}
		}
	}
	
	@Test
	public void shouldCheckIfItemCanBePartiallyCancelled() {
		for (Status status : OrderItem.CANCELLABLE_STATES) {
			item.setStatus(status);
			assertTrue(item.canCancel(item.getQuantity() - 1, true));
		}
	}
	
	@Test
	public void shouldApproveItemIfCanBeApproved() {
		doReturn(true).when(item).canApprove();
		item.approve();
		assertEquals(item.getStatus(), Status.approved);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotApproveItemIfCantBeApproved() {
		doReturn(false).when(item).canApprove();
		item.approve();
	}
	
	@Test
	public void shouldConfirmItemIfCanBeConfirmed() {
		doReturn(true).when(item).canConfirm();
		item.confirm();
		assertEquals(item.getStatus(), Status.confirmed);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotConfirmItemIfCantBeConfirmed() {
		doReturn(false).when(item).canConfirm();
		item.confirm();
	}
	
	@Test
	public void shouldCancelItemIfCanBeCancelled() {
		doReturn(true).when(item).canCancel(2, true);
		item.cancel(2, "test reason");
		assertEquals(item.getStatus(), Status.cancelled);
		assertEquals(item.getQuantity(), 0);
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotCancelItemIfCantBeCancelled() {
		doThrow(OMSException.class).when(item).canCancel(2, true);
		item.cancel(2, "test reason");
	}
	
	@Test
	public void shouldCancelPartialItemIfCanBeCancelled() {
		doReturn(true).when(item).canCancel(1, true);
		item.cancel(1, "test reason");
		assertEquals(item.getStatus(), Status.created);
		assertEquals(item.getQuantity(), 1);
	}
	
	@Test
	public void shouldCreateItemRequestOnCancellation() {
		doReturn(true).when(item).canCancel(2, true);
		item.cancel(2, "test reason");
		OrderItemRequest request = item.getRequests().iterator().next();
		assertEquals(request.getQuantity(), 2);
		assertEquals(request.getSuccessfulQuantity(), 2);
		assertEquals(request.getReason(), "test reason");
	}
	
	@Test
	public void shouldCreateItemRequestOnPartialCancellation() {
		doReturn(true).when(item).canCancel(1, true);
		item.cancel(1, "test reason");
		OrderItemRequest request = item.getRequests().iterator().next();
		assertEquals(request.getQuantity(), 1);
		assertEquals(request.getSuccessfulQuantity(), 1);
		assertEquals(request.getReason(), "test reason");
	}
	
	@Test
	public void shouldAddNote() {
		OrderItemNote note = new OrderItemNote("test123");
		item.addNote(note);
		assertTrue(item.getNotes().contains(note));
	}
	
	@Test
	public void shouldRemoveNote() {
		OrderItemNote note = new OrderItemNote("test123");
		item.addNote(note);
		item.removeNote(note);
		assertFalse(item.getNotes().contains(note));
	}
	
	@Test
	public void shouldAddRequest() {
		OrderItemRequest request = new OrderItemRequest(RequestType.cancel, 2, 2, "Buyer cancelled");
		item.addRequest(request);
		assertTrue(item.getRequests().contains(request));
	}
	
	@Test
	public void shouldRemoveRequest() {
		OrderItemRequest request = new OrderItemRequest(RequestType.cancel, 2, 2, "Buyer cancelled");
		item.addRequest(request);
		item.removeRequest(request);
		assertFalse(item.getRequests().contains(request));
	}
	
	@Test
	public void shouldAddAdjustmentsIfCostCanBeComputed() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertTrue(item.getAdjustments().contains(adjustment));
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotAddAdjustmentsIfCostCantBeComputed() {
		doReturn(false).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
	}
	
	@Test
	public void shouldRemoveAdjustmentsIfCostCanBeComputed() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.removeAdjustment(adjustment);
		assertFalse(item.getAdjustments().contains(adjustment));
	}
	
	@Test(expectedExceptions=OMSException.class)
	public void shouldNotRemoveAdjustmentsIfCostCantBeComputed() {
		doReturn(false).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.removeAdjustment(adjustment);
	}
	
	@Test
	public void shouldComputeTotalCostAfterAddingAdjustments() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertEquals(item.getTotal(), item.getQuantity() * (item.getPrice() + 10d));
	}
	
	@Test
	public void shouldComputeTotalCost() {
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		assertEquals(item.computeTotalCost(), item.getQuantity() * (item.getPrice() + 10d));
	}
	
	@Test
	public void shouldComputeTotalCostAfterRemovingAdjustments() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.removeAdjustment(adjustment);
		assertEquals(item.getTotal(), item.getQuantity() * item.getPrice());
	}
	
	@Test
	public void shouldComputeTotalCostAfterCancellingItem() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.cancel(2, "test reason");
		assertEquals(item.getTotal(), 0d);
	}
	
	@Test
	public void shouldComputeTotalCostAfterCancellingPartialItem() {
		doReturn(true).when(item).canComputeTotalCost();
		OrderItemAdjustment adjustment = new OrderItemAdjustment(Type.shipping, 10d);
		item.addAdjustment(adjustment);
		item.cancel(1, "test reason");
		assertEquals(item.getTotal(), item.getQuantity() * (item.getPrice() + 10d));
	}
}

/**
 * 
 */
package com.sugarify.oms;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.server.exception.ApplicationException;

/**
 * @author ganeshs
 *
 */
public class OMSException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	/**
	 * @author ganeshs
	 *
	 */
	public enum ErrorCode {
		
		bad_state(HttpResponseStatus.UNPROCESSABLE_ENTITY), insufficient_quantity(HttpResponseStatus.UNPROCESSABLE_ENTITY);
		
		private HttpResponseStatus status;
		
		/**
		 * @param status
		 */
		private ErrorCode(HttpResponseStatus status) {
			this.status = status;
		}
	}

	/**
	 * @param code
	 * @param message
	 */
	public OMSException(ErrorCode code, String message) {
		super(code.status, message);
	}
	
	/**
	 * @param code
	 * @param message
	 * @param cause
	 */
	public OMSException(ErrorCode code, String message, Throwable cause) {
		super(code.status, message, cause);
	}
}

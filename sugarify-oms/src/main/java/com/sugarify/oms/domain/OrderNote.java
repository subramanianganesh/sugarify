/**
 * 
 */
package com.sugarify.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @createdBy ganeshs
 *
 */
@Entity
@Table(name="order_notes")
@Access(AccessType.FIELD)
public class OrderNote extends BaseRecordableDomain {

	@NotEmpty
	private String note;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderId")
	@JsonBackReference("notes")
	private Order order;
	
	private String createdBy;
	
	/**
	 * Default constructor
	 */
	public OrderNote() {
	}

	/**
	 * @param note
	 */
	public OrderNote(String note) {
		this.note = note;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}

/**
 * 
 */
package com.sugarify.oms.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.minnal.instrument.entity.Action;
import org.minnal.instrument.entity.AggregateRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.common.collect.Sets;
import com.sugarify.core.domain.BaseRecordableDomain;
import com.sugarify.oms.OMSException;
import com.sugarify.oms.OMSException.ErrorCode;

/**
 * @author ganeshs
 *
 */
@AggregateRoot
@Entity
@Table(name="orders")
@Access(AccessType.FIELD)
public class Order extends BaseRecordableDomain {
	
	/**
	 * @author ganeshs
	 *
	 */
	public enum Status {
		created, approved, confirmed, dispatched, shipped, delivered, cancelled, returned, completed, on_hold
	}

	@NotNull
	private Long userId;
	
	@NotNull
	private Long shippingAddressId;
	
	@NotNull
	private Long billingAddressId;
	
	@OneToMany(mappedBy="order", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("items")
	private Set<OrderItem> items = new HashSet<OrderItem>();
	
	@OneToMany(mappedBy="order", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("adjustments")
	private Set<OrderAdjustment> adjustments = new HashSet<OrderAdjustment>();
	
	@OneToMany(mappedBy="order", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("notes")
	private Set<OrderNote> notes = new HashSet<OrderNote>();
	
	@OneToMany(mappedBy="order", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("requests")
	private Set<OrderRequest> requests = new HashSet<OrderRequest>();
	
	private Double total = 0d;
	
	@Enumerated(EnumType.STRING)
	private Status status = Status.created;
	
	private String externalId;
	
	@ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="order_attributes", joinColumns=@JoinColumn(name="orderId"))
	private Map<String, String> attributes = new HashMap<String, String>();
	
	private Boolean deleted = false;
	
	@NotNull
	private Timestamp orderDate;
	
	private static final Logger logger = LoggerFactory.getLogger(Order.class);
	
	public static final Set<Status> TOTAL_COST_COMPUTABLE_STATES = Sets.newHashSet(Status.created, Status.on_hold, Status.approved, Status.confirmed);
	
	public static final Set<Status> CANCELLABLE_STATES = Sets.newHashSet(Status.created, Status.on_hold, Status.approved, Status.confirmed);
	
	public static final Set<Status> APPROVABLE_STATES = Sets.newHashSet(Status.created, Status.on_hold);
	
	/**
	 * Default constructor
	 */
	public Order() {
	}

	/**
	 * @param userId
	 * @param externalId
	 * @param shippingAddressId
	 * @param billingAddressId
	 * @param orderDate
	 */
	public Order(Long userId, String externalId, Long shippingAddressId, Long billingAddressId, Timestamp orderDate) {
		this.userId = userId;
		this.shippingAddressId = shippingAddressId;
		this.billingAddressId = billingAddressId;
		this.externalId = externalId;
		this.orderDate = orderDate;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the shippingAddressId
	 */
	public Long getShippingAddressId() {
		return shippingAddressId;
	}

	/**
	 * @param shippingAddressId the shippingAddressId to set
	 */
	public void setShippingAddressId(Long shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}

	/**
	 * @return the billingAddressId
	 */
	public Long getBillingAddressId() {
		return billingAddressId;
	}

	/**
	 * @param billingAddressId the billingAddressId to set
	 */
	public void setBillingAddressId(Long billingAddressId) {
		this.billingAddressId = billingAddressId;
	}

	/**
	 * @return the items
	 */
	public Set<OrderItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Set<OrderItem> items) {
		this.items = items;
	}

	/**
	 * @return the adjustments
	 */
	public Set<OrderAdjustment> getAdjustments() {
		return adjustments;
	}

	/**
	 * @param adjustments the adjustments to set
	 */
	public void setAdjustments(Set<OrderAdjustment> adjustments) {
		this.adjustments = adjustments;
	}

	/**
	 * @return the requests
	 */
	public Set<OrderRequest> getRequests() {
		return requests;
	}

	/**
	 * @param requests the requests to set
	 */
	public void setRequests(Set<OrderRequest> requests) {
		this.requests = requests;
	}

	/**
	 * @return the notes
	 */
	public Set<OrderNote> getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Set<OrderNote> notes) {
		this.notes = notes;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * @param externalId the externalId to set
	 */
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * @return the attributes
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	
	/**
	 * @return the deleted
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the orderDate
	 */
	public Timestamp getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}
	
	/**
	 * Adds the note to the order
	 * 
	 * @param note
	 */
	public void addNote(OrderNote note) {
		logger.debug("Adding the note {} to the order {}", note, this);
		note.setOrder(this);
		notes.add(note);
	}
	
	/**
	 * Removes the note from the order
	 * 
	 * @param note
	 */
	public void removeNote(OrderNote note) {
		logger.debug("Removing the note {} from the order {}", note, this);
		notes.remove(note);
	}
	
	/**
	 * Adds the request to the order
	 * 
	 * @param request
	 */
	public void addRequest(OrderRequest request) {
		logger.debug("Adding the request {} to the order {}", request, this);
		request.setOrder(this);
		requests.add(request);
	}
	
	/**
	 * Removes the request from the order
	 * 
	 * @param request
	 */
	public void removeRequest(OrderRequest request) {
		logger.debug("Removing the request {} from the order {}", request, this);
		requests.remove(request);
	}

	/**
	 * Adds the item to the order
	 * 
	 * @param item
	 */
	public void addItem(OrderItem item) {
		logger.debug("Adding item {} to the the order {}", item, this);
		if (status != Status.created) {
			logger.error("Order {} not in created state. Can't add item {}", this, item);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in created state");
		}
		item.setOrder(this);
		items.add(item);
		computeTotalCost();
	}
	
	/**
	 * Removes the item from the order 
	 * @param item
	 */
	public void removeItem(OrderItem item) {
		logger.debug("Removing item {} from the the order {}", item, this);
		if (status != Status.created) {
			logger.error("Order {} not in created state. Can't remove item {}", this, item);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in created state");
		}
		items.remove(item);
		computeTotalCost();
	}
	
	/**
	 * Adds the adjustment to the order
	 *  
	 * @param adjustment
	 */
	public void addAdjustment(OrderAdjustment adjustment) {
		logger.debug("Adding adjustment {} to the the order {}", adjustment, this);
		if (! canComputeTotalCost()) {
			logger.error("Order {} not in valid state. Can't add adjustment {}", this, adjustment);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		adjustment.setOrder(this);
		adjustments.add(adjustment);
		computeTotalCost();
	}
	
	/**
	 * Removes the adjustment from the order 
	 * 
	 * @param adjustment
	 */
	public void removeAdjustment(OrderAdjustment adjustment) {
		logger.debug("Removing adjustment {} from the the order {}", adjustment, this);
		if (! canComputeTotalCost()) {
			logger.error("Order {} not in valid state. Can't remove adjustment {}", this, adjustment);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		adjustments.remove(adjustment);
		computeTotalCost();
	}
	
	/**
	 * Calculates the total cost of the order
	 */
	public Double computeTotalCost() {
		logger.debug("Computing the total cost of the order {}", this);
		if (! canComputeTotalCost()) {
			logger.debug("Order {} not in valid state to compute the total cost", this);
			return total;
		}
		total = 0d;
		for (OrderItem item : items) {
			total += item.computeTotalCost();
		}
		for (OrderAdjustment adjustment : adjustments) {
			total += adjustment.getValue();
		}
		return total;
	}
	
	/**
	 * Computes the current status of the order
	 * 
	 * @return
	 */
	protected Status computeStatus() {
		Set<OrderItem.Status> statuses = new HashSet<OrderItem.Status>();
		for (OrderItem item : getItems()) {
			statuses.add(item.getStatus());
		}
		if (statuses.contains(OrderItem.Status.on_hold)) {
			this.status = Status.on_hold;
		} else if (statuses.contains(OrderItem.Status.approved)) {
			this.status = Status.approved;
		} else if (statuses.contains(OrderItem.Status.confirmed)) {
			this.status = Status.confirmed;
		} else if (statuses.contains(OrderItem.Status.dispatched)) {
			this.status = Status.dispatched;
		} else if (statuses.contains(OrderItem.Status.shipped)) {
			this.status = Status.shipped;
		} else if (statuses.contains(OrderItem.Status.delivered)) {
			this.status = Status.delivered;
		} else if (statuses.contains(OrderItem.Status.returned)) {
			this.status = Status.returned;
		} else if (statuses.contains(OrderItem.Status.cancelled)) {
			this.status = Status.cancelled;
		}
		return this.status;
	}
	
	/**
	 * Approves the order
	 */
	@Action(value="approve")
	public void approve() {
		logger.debug("Approving the order {}", this);
		if (! canApprove()) {
			logger.error("Order {} not in valid state to be approved", this);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		for (OrderItem item : items) {
			item.approve();
		}
		computeStatus();
		persist();
	}
	
	/**
	 * Confirms the order
	 */
	@Action(value="confirm")
	public void confirm() {
		logger.debug("Confirming the order {}", this);
		if (! canConfirm()) {
			logger.error("Order {} can be confirmed only in approved state", this);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		for (OrderItem item : items) {
			item.confirm();
		}
		computeStatus();
		persist();
	}
	
	@Action(value="confirm", path="items")
	public void confirmItem(OrderItem item) {
		logger.debug("Confirming the order item {}", item);
		if (! canConfirm()) {
			logger.error("Order {} can be confirmed only in approved state", this);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		item.confirm();
		computeStatus();
		persist();
	}
	
	/**
	 * Cancels the order
	 * 
	 * @param reason
	 */
	@Action(value="cancel")
	public void cancel(String reason) {
		logger.debug("Cancelling the order {} with the reason {}", this, reason);
		if (! canCancel()) {
			logger.error("Order {} not in valid state to be cancelled", this);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		addRequest(new OrderRequest(RequestType.cancel, reason));
		for (OrderItem item : items) {
			item.cancel(item.getQuantity(), reason);
		}
//		setStatus(Status.cancelled);
		computeTotalCost();
		computeStatus();
		persist();
	}
	
	/**
	 * Cancels the order item with the specified quantity and reason
	 * 
	 * @param item
	 * @param quantity
	 * @param reason
	 */
	@Action(value="cancel", path="items")
	public void cancelItem(OrderItem item, int quantity, String reason) {
		logger.debug("Cancelling the order item {} with the reason {} and quantity {}", this, reason, quantity);
		if (! canCancel()) {
			logger.error("Order {} not in valid state to be cancelled", this);
			throw new OMSException(ErrorCode.bad_state, "Order " + getId() + " not in valid state");
		}
		item.cancel(quantity, reason);
		computeTotalCost();
		computeStatus();
		persist();
	}
	
	/**
	 * Checks if the order can be approved
	 * 
	 * @return
	 */
	public boolean canApprove() {
		return APPROVABLE_STATES.contains(getStatus());
	}
	
	/**
	 * Checks if the order can be confirmed
	 * 
	 * @return
	 */
	public boolean canConfirm() {
		return status == Status.approved;
	}
	
	/**
	 * Checks if the order can be cancelled
	 * 
	 * @return
	 */
	public boolean canCancel() {
		return CANCELLABLE_STATES.contains(getStatus());
	}
	
	/**
	 * @return
	 */
	public boolean canComputeTotalCost() {
		return TOTAL_COST_COMPUTABLE_STATES.contains(getStatus());
	}
	
	@PrePersist
	void computeCost() {
		computeTotalCost();
	}
}

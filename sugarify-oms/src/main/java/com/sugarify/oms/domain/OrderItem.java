/**
 * 
 */
package com.sugarify.oms.domain;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.common.collect.Sets;
import com.sugarify.core.domain.BaseRecordableDomain;
import com.sugarify.oms.OMSException;
import com.sugarify.oms.OMSException.ErrorCode;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_items")
@Access(AccessType.FIELD)
public class OrderItem extends BaseRecordableDomain {
	
	/**
	 * @author ganeshs
	 *
	 */
	public enum Status {
		on_hold, created, approved, confirmed, dispatched, shipped, delivered, returned, cancelled
	}
	
	@Enumerated(EnumType.STRING)
	private Status status = Status.created;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderId")
	@JsonBackReference("items")
	private Order order;
	
	@ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="order_item_attributes", joinColumns=@JoinColumn(name="orderItemId"))
	private Map<String, String> attributes = new HashMap<String, String>();
	
	@OneToMany(mappedBy="item", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("adjustments")
	private Set<OrderItemAdjustment> adjustments = new HashSet<OrderItemAdjustment>();
	
	@OneToMany(mappedBy="item", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("notes")
	private Set<OrderItemNote> notes = new HashSet<OrderItemNote>();
	
	@OneToMany(mappedBy="item", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("requests")
	private Set<OrderItemRequest> requests = new HashSet<OrderItemRequest>();
	
	@NotNull
	private Long merchantId;
	
	@NotNull
	private Long merchantProductId;
	
	@NotNull
	private Long variantId;
	
	@Min(value=0)
	private int quantity;
	
	@NotEmpty
	private String title;
	
	private String description;
	
	@NotNull
	private Timestamp orderDate;
	
	@Min(value=0)
	private Double price;
	
	private Double total;
	
	private Boolean deleted = false;
	
	private static final Logger logger = LoggerFactory.getLogger(OrderItem.class);
	
	public static final Set<Status> CANCELLABLE_STATES = Sets.newHashSet(Status.created, Status.approved, Status.confirmed, Status.on_hold);
	
	public static final Set<Status> TOTAL_COST_COMPUTABLE_STATES = Sets.newHashSet(Status.created, Status.approved, Status.confirmed, Status.on_hold);
	
	public static final Set<Status> APPROVABLE_STATES = Sets.newHashSet(Status.created, Status.on_hold);
	
	/**
	 * Default constructor
	 */
	public OrderItem() {
	}

	/**
	 * @param merchantId
	 * @param merchantProductId
	 * @param variantId
	 * @param quantity
	 * @param title
	 * @param description
	 * @param price
	 * @param orderDate
	 */
	public OrderItem(Long merchantId, Long merchantProductId, Long variantId,
			int quantity, String title, String description, Double price, Timestamp orderDate) {
		this.merchantId = merchantId;
		this.merchantProductId = merchantProductId;
		this.variantId = variantId;
		this.quantity = quantity;
		this.title = title;
		this.description = description;
		this.orderDate = orderDate;
		this.price = price;
	}

	/**
	 * @return the order
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @return the attributes
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the adjustments
	 */
	public Set<OrderItemAdjustment> getAdjustments() {
		return adjustments;
	}

	/**
	 * @param adjustments the adjustments to set
	 */
	public void setAdjustments(Set<OrderItemAdjustment> adjustments) {
		this.adjustments = adjustments;
	}

	/**
	 * @return the merchantId
	 */
	public Long getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the merchantProductId
	 */
	public Long getMerchantProductId() {
		return merchantProductId;
	}

	/**
	 * @param merchantProductId the merchantProductId to set
	 */
	public void setMerchantProductId(Long merchantProductId) {
		this.merchantProductId = merchantProductId;
	}

	/**
	 * @return the variantId
	 */
	public Long getVariantId() {
		return variantId;
	}

	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(Long variantId) {
		this.variantId = variantId;
	}
	
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the deleted
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the orderDate
	 */
	public Timestamp getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the notes
	 */
	public Set<OrderItemNote> getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Set<OrderItemNote> notes) {
		this.notes = notes;
	}

	/**
	 * @return the requests
	 */
	public Set<OrderItemRequest> getRequests() {
		return requests;
	}

	/**
	 * @param requests the requests to set
	 */
	public void setRequests(Set<OrderItemRequest> requests) {
		this.requests = requests;
	}

	/**
	 * @return the total
	 */
	public Double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Double total) {
		this.total = total;
	}
	
	/**
	 * Adds the note to the order item
	 * 
	 * @param note
	 */
	public void addNote(OrderItemNote note) {
		logger.debug("Adding the note {} to the order item {}", note, this);
		note.setItem(this);
		notes.add(note);
	}
	
	/**
	 * Removes the note from the order item
	 * 
	 * @param note
	 */
	public void removeNote(OrderItemNote note) {
		logger.debug("Removing the note {} from the order item {}", note, this);
		notes.remove(note);
	}
	
	/**
	 * Adds the request to the order item
	 * 
	 * @param request
	 */
	public void addRequest(OrderItemRequest request) {
		logger.debug("Adding the note {} to the order item {}", request, this);
		request.setItem(this);
		requests.add(request);
	}
	
	/**
	 * Removes the request from the order item
	 * 
	 * @param request
	 */
	public void removeRequest(OrderItemRequest request) {
		logger.debug("Removing the request {} from the order item {}", request, this);
		requests.remove(request);
	}


	/**
	 * Adds the adjustment to the order item
	 *  
	 * @param adjustment
	 */
	public void addAdjustment(OrderItemAdjustment adjustment) {
		logger.debug("Adding the adjustment {} to the order item {}", adjustment, this);
		if (! canComputeTotalCost()) {
			logger.error("Order item {} not in a valid state. Can't add adjustment {}", this, adjustment);
			throw new OMSException(ErrorCode.bad_state, "Order item " + getId() + " not in valid state");
		}
		adjustment.setItem(this);
		adjustments.add(adjustment);
		computeTotalCost();
	}
	
	/**
	 * Removes the adjustment from the order item
	 *  
	 * @param adjustment
	 */
	public void removeAdjustment(OrderItemAdjustment adjustment) {
		logger.debug("Removing the adjustment {} from the order item {}", adjustment, this);
		if (! canComputeTotalCost()) {
			logger.error("Order item {} not in a valid state. Can't remove adjustment {}", this, adjustment);
			throw new OMSException(ErrorCode.bad_state, "Order item " + getId() + " not in valid state");
		}
		adjustments.remove(adjustment);
		computeTotalCost();
	}
	
	/**
	 * Calculate the total cost
	 */
	public Double computeTotalCost() {
		logger.debug("Computing the total cost for the item {}", this);
		if (! canComputeTotalCost()) {
			logger.debug("Order item {} not in valid state to compute total", this);
			return total;
		}
		total = price * quantity;
		for (OrderItemAdjustment adjustment : adjustments) {
			total += adjustment.valueFor(quantity);
		}
		return total;
	}
	
	/**
	 * Confirms the order item
	 */
	public void confirm() {
		logger.debug("Confirming the order item {}", this);
		if (! canConfirm()) {
			logger.error("Order item {} is not in a valid state to confirm", this);
			throw new OMSException(ErrorCode.bad_state, "Order item " + getId() + " not in valid state");
		}
		setStatus(Status.confirmed);
	}
	
	/**
	 * Cancel the specified number of quantities
	 * 
	 * @param quantity
	 * @param reason
	 */
	public void cancel(int quantity, String reason) {
		logger.debug("Cancelling {} quantities from the item {} with the reason {}", quantity, this, reason);
		if (canCancel(quantity, true)) {
			addRequest(new OrderItemRequest(RequestType.cancel, quantity, quantity, reason));
			int newQuantity = getQuantity() - quantity;
			setQuantity(newQuantity);
			if (newQuantity == 0) {
				setStatus(Status.cancelled);
			}
		}
		computeTotalCost();
	}
	
	/**
	 * Approves the order item
	 */
	public void approve() {
		logger.debug("Approving the order item {}", this);
		if (! canApprove()) {
			logger.error("Item {} not in valid state. Cant't approve", this);
			throw new OMSException(ErrorCode.bad_state, "Item " + getId() + " is not in valid state");
		}
		setStatus(Status.approved);
	}
	
	/**
	 * @return
	 */
	public boolean canConfirm() {
		return getStatus() == Status.approved;
	}
	
	/**
	 * Checks the item with specified quantities can be cancelled
	 * 
	 * @param quantity
	 * @return
	 */
	public boolean canCancel(int quantity) {
		return canCancel(quantity, false);
	}
	
	/**
	 * Checks if the item can be approved
	 * 
	 * @return
	 */
	public boolean canApprove() {
		return APPROVABLE_STATES.contains(getStatus());
	}
	
	/**
	 * Checks the item with specified quantities can be cancelled
	 * 
	 * @param quantity
	 */
	public boolean canCancel(int quantity, boolean raiseException) {
		if (! CANCELLABLE_STATES.contains(getStatus())) {
			if (raiseException) {
				throw new OMSException(ErrorCode.bad_state, "Item " + getId() + " not in one of the valid states - " + CANCELLABLE_STATES.toString());
			}
			return false;
		}
		if (getQuantity() - quantity < 0) {
			if (raiseException) {
				throw new OMSException(ErrorCode.insufficient_quantity, "Item " + getId() + " doesn't have sufficient quantities to cancel");
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if the order item total value can be computed
	 * 
	 * @return
	 */
	public boolean canComputeTotalCost() {
		return TOTAL_COST_COMPUTABLE_STATES.contains(getStatus());
	}
}

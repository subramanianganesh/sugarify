/**
 * 
 */
package com.sugarify.oms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="order_item_requests")
@Access(AccessType.FIELD)
public class OrderItemRequest extends BaseRecordableDomain {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orderItemId")
	@JsonBackReference("requests")
	private OrderItem item;
	
	@Min(value=0)
	private int quantity;
	
	@Min(value=0)
	private int successfulQuantity;
	
	private String reason;
	
	private String createdBy;
	
	@Enumerated(EnumType.STRING)
	private RequestType type;
	
	/**
	 * Default constructor
	 */
	public OrderItemRequest() {
	}

	/**
	 * @param type
	 * @param quantity
	 * @param successfulQuantity
	 * @param reason
	 */
	public OrderItemRequest(RequestType type, int quantity, int successfulQuantity, String reason) {
		this.quantity = quantity;
		this.successfulQuantity = successfulQuantity;
		this.reason = reason;
		this.type = type;
	}

	/**
	 * @return the item
	 */
	public OrderItem getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(OrderItem item) {
		this.item = item;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the successfulQuantity
	 */
	public int getSuccessfulQuantity() {
		return successfulQuantity;
	}

	/**
	 * @param successfulQuantity the successfulQuantity to set
	 */
	public void setSuccessfulQuantity(int successfulQuantity) {
		this.successfulQuantity = successfulQuantity;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the type
	 */
	public RequestType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(RequestType type) {
		this.type = type;
	}
}

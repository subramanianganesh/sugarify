/**
 * 
 */
package com.sugarify.oms.domain;

/**
 * @author ganeshs
 *
 */
public enum RequestType {

	cancel, hold
}

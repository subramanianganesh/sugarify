/**
 * 
 */
package com.sugarify.cms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * A value of an option type. For ex: if size is the option type, value could be small, medium or large
 * 
 * @author ganeshs
 *
 */
@Entity
@Table(name="option_values")
@Access(AccessType.FIELD)
public class OptionValue extends BaseRecordableDomain {

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="typeId")
	@JsonBackReference
	private OptionType type;
	
	private int position;
	
	@NotEmpty
	private String value;
	
	@NotEmpty
	private String displayValue;

	/**
	 * @return the type
	 */
	public OptionType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(OptionType type) {
		this.type = type;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the displayValue
	 */
	public String getDisplayValue() {
		return displayValue;
	}

	/**
	 * @param displayValue the displayValue to set
	 */
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}
}

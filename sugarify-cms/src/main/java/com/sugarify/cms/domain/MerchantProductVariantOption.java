/**
 * 
 */
package com.sugarify.cms.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="merchant_product_variant_options")
@Access(AccessType.FIELD)
public class MerchantProductVariantOption extends BaseRecordableDomain {
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="variantId")
	@JsonBackReference("options")
	private MerchantProductVariant variant;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="typeId", insertable=false, updatable=false)
	@JsonIgnore
	private OptionType type;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="valueId", insertable=false, updatable=false)
	@JsonIgnore
	private OptionValue value;
	
	private Long typeId;
	
	private Long valueId;

	/**
	 * @return the variant
	 */
	public MerchantProductVariant getVariant() {
		return variant;
	}

	/**
	 * @param variant the variant to set
	 */
	public void setVariant(MerchantProductVariant variant) {
		this.variant = variant;
	}

	/**
	 * @return the type
	 */
	public OptionType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(OptionType type) {
		this.type = type;
		this.typeId = type.getId();
	}

	/**
	 * @return the value
	 */
	public OptionValue getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(OptionValue value) {
		this.value = value;
		this.valueId = value.getId();
	}

	/**
	 * @return the typeId
	 */
	public Long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the valueId
	 */
	public Long getValueId() {
		return valueId;
	}

	/**
	 * @param valueId the valueId to set
	 */
	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}
}

/**
 * 
 */
package com.sugarify.cms.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.minnal.instrument.entity.AggregateRoot;
import org.minnal.instrument.entity.Searchable;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@AggregateRoot
@Entity
@Table(name="merchant_products")
@Access(AccessType.FIELD)
public class MerchantProduct extends BaseRecordableDomain {

	@Searchable
	private Long merchantId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="productId", insertable=false, updatable=false)
	@Transient
	private Product product;
	
	@Searchable
	private Long productId;
	
	@NotNull
	private Double costPrice;
	
	@NotNull
	private Double salePrice;
	
	private boolean enabled = false;
	
	private String metaDescription;
	
	private String metaKeywords;
	
	@OneToMany(mappedBy="merchantProduct", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference
	private Set<MerchantProductVariant> variants = new HashSet<MerchantProductVariant>();

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the costPrice
	 */
	public Double getCostPrice() {
		return costPrice;
	}

	/**
	 * @param costPrice the costPrice to set
	 */
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the metaDescription
	 */
	public String getMetaDescription() {
		return metaDescription;
	}

	/**
	 * @param metaDescription the metaDescription to set
	 */
	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	/**
	 * @return the metaKeywords
	 */
	public String getMetaKeywords() {
		return metaKeywords;
	}

	/**
	 * @param metaKeywords the metaKeywords to set
	 */
	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}

	/**
	 * @return the variants
	 */
	public Set<MerchantProductVariant> getVariants() {
		return variants;
	}

	/**
	 * @param variants the variants to set
	 */
	public void setVariants(Set<MerchantProductVariant> variants) {
		this.variants = variants;
	}

	/**
	 * @param variant
	 */
	public void addVariant(MerchantProductVariant variant) {
		variant.setMerchantProduct(this);
		this.variants.add(variant);
	}
	
	/**
	 * @param variant
	 */
	public void removeVariant(MerchantProductVariant variant) {
		this.variants.remove(variant);
	}

	/**
	 * @return the merchantId
	 */
	public Long getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
}

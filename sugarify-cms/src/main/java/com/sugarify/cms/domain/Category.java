/**
 * 
 */
package com.sugarify.cms.domain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.minnal.instrument.entity.AggregateRoot;
import org.minnal.instrument.entity.Searchable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@AggregateRoot
@Entity
@Table(name="categories")
@Access(AccessType.FIELD)
public class Category extends BaseRecordableDomain {

	private String name;
	
	private String description;
	
	private String imageUrl;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parentId")
	@JsonBackReference
	private Category parent;
	
	@OneToMany(mappedBy="parent", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference
	private Set<Category> children = new HashSet<Category>();
	
	@Searchable
	private Boolean enabled = true;
	
	@ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="category_attributes", joinColumns=@JoinColumn(name="categoryId"))
	private Map<String, String> attributes = new HashMap<String, String>();
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the attributes
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the parent
	 */
	public Category getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Category parent) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	public Set<Category> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(Set<Category> children) {
		this.children = children;
	}
	
	/**
	 * Adds the child to the list
	 * 
	 * @param child
	 */
	public void addChild(Category child) {
		child.setParent(this);
		children.add(child);
	}
	
	/**
	 * Removes the child from the list
	 * 
	 * @param child
	 */
	public void removeChild(Category child) {
		children.remove(child);
	}

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}

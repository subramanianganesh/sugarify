/**
 * 
 */
package com.sugarify.cms.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.minnal.instrument.entity.Searchable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * @author ganeshs
 *
 */
@Entity
@Table(name="merchant_product_variants")
@Access(AccessType.FIELD)
public class MerchantProductVariant extends BaseRecordableDomain {
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="merchantProductId")
	@JsonBackReference
	private MerchantProduct merchantProduct;
	
	@NotEmpty
	@Searchable
	private String sku;
	
	@NotNull
	private Double costPrice;
	
	@NotNull
	private Double salePrice;
	
	private boolean enabled = false;
	
	private int position;
	
	private boolean trackInventory = false;
	
	@OneToMany(mappedBy="variant", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("options")
	private Set<MerchantProductVariantOption> options = new HashSet<MerchantProductVariantOption>();
	
	@OneToMany(mappedBy="variant", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference("inventories")
	private Set<Inventory> inventories = new HashSet<Inventory>();

	/**
	 * @return the merchantProduct
	 */
	public MerchantProduct getMerchantProduct() {
		return merchantProduct;
	}

	/**
	 * @param merchantProduct the merchantProduct to set
	 */
	public void setMerchantProduct(MerchantProduct merchantProduct) {
		this.merchantProduct = merchantProduct;
	}

	/**
	 * @return the sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	/**
	 * @return the costPrice
	 */
	public Double getCostPrice() {
		return costPrice;
	}

	/**
	 * @param costPrice the costPrice to set
	 */
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	/**
	 * @return the salePrice
	 */
	public Double getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the trackInventory
	 */
	public boolean isTrackInventory() {
		return trackInventory;
	}

	/**
	 * @param trackInventory the trackInventory to set
	 */
	public void setTrackInventory(boolean trackInventory) {
		this.trackInventory = trackInventory;
	}

	/**
	 * @return the options
	 */
	public Set<MerchantProductVariantOption> getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(Set<MerchantProductVariantOption> options) {
		this.options = options;
	}

	/**
	 * @return the inventories
	 */
	public Set<Inventory> getInventories() {
		return inventories;
	}

	/**
	 * @param inventories the inventories to set
	 */
	public void setInventories(Set<Inventory> inventories) {
		this.inventories = inventories;
	}

	/**
	 * @param inventory
	 */
	public void addInventory(Inventory inventory) {
		inventory.setVariant(this);
		this.inventories.add(inventory);
	}
	
	/**
	 * @param inventory
	 */
	public void removeInventory(Inventory inventory) {
		this.inventories.remove(inventory);
	}

	/**
	 * @param option
	 */
	public void addOption(MerchantProductVariantOption option) {
		option.setVariant(this);
		this.options.add(option);
	}
	
	/**
	 * @param option
	 */
	public void removeOption(MerchantProductVariantOption option) {
		this.options.remove(option);
	}
}

/**
 * 
 */
package com.sugarify.cms.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
import org.minnal.instrument.entity.AggregateRoot;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sugarify.core.domain.BaseRecordableDomain;

/**
 * Option Types are a way to help distinguish products in your store from one another. They are particularly useful when you have many products 
 * that are basically of the same general category (Tshirts or mugs, for example), but with characteristics that can vary, such as color, size, or logo.
 * 
 * <p>For each Option Type, you will need to create one or more corresponding Option Values. If you create a 'Size' Option Type, 
 * then you would need Option Values for it like 'Small', 'Medium', and 'Large'
 * 
 * @author ganeshs
 *
 */
@AggregateRoot
@Entity
@Table(name="option_types")
@Access(AccessType.FIELD)
public class OptionType extends BaseRecordableDomain {

	@NotEmpty
	private String name;
	
	@NotEmpty
	private String displayName;
	
	@OneToMany(mappedBy="type", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonManagedReference
	private Set<OptionValue> values = new HashSet<OptionValue>();

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the values
	 */
	public Set<OptionValue> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(Set<OptionValue> values) {
		this.values = values;
	}
	
	/**
	 * @param value
	 */
	public void addValue(OptionValue value) {
		value.setType(this);
		this.values.add(value);
	}
	
	/**
	 * @param value
	 */
	public void removeValue(OptionValue value) {
		this.values.remove(value);
	}
}

package com.sugarify.core.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;

import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class UserResourceTest extends BaseJPAResourceTest {
	@Test
	public void listUserTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		Response response = call(request("/users/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.core.domain.User.class).size(),
				(int) com.sugarify.core.domain.User.count());
	}

	@Test
	public void readUserTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		Response response = call(request("/users/" + user.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.core.domain.User.class).getId(),
				user.getId());
	}

	@Test
	public void createUserTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		Response response = call(request("/users/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(user)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateUserTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		com.sugarify.core.domain.User modifieduser = createDomain(
				com.sugarify.core.domain.User.class, 1);
		Response response = call(request("/users/" + user.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifieduser)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		user.merge();
		assertTrue(compare(modifieduser, user, 1));
	}

	@Test
	public void deleteUserTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		Response response = call(request("/users/" + user.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/users/" + user.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(user)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listUserAddressTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();

		com.sugarify.core.domain.Address address = createDomain(com.sugarify.core.domain.Address.class);
		user.collection("addresses").add(address);
		user.persist();

		Response response = call(request("/users/" + user.getId()
				+ "/addresses/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.core.domain.Address.class).size(),
				user.getAddresses().size());
	}

	@Test
	public void readUserAddressTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		com.sugarify.core.domain.Address address = createDomain(com.sugarify.core.domain.Address.class);
		user.collection("addresses").add(address);
		user.persist();
		Response response = call(request("/users/" + user.getId()
				+ "/addresses/" + address.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.core.domain.Address.class).getId(),
				address.getId());
	}

	@Test
	public void createUserAddressTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		com.sugarify.core.domain.Address address = createDomain(com.sugarify.core.domain.Address.class);
		Response response = call(request("/users/" + user.getId()
				+ "/addresses/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(address)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateUserAddressTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		com.sugarify.core.domain.Address address = createDomain(com.sugarify.core.domain.Address.class);
		user.collection("addresses").add(address);
		user.persist();
		com.sugarify.core.domain.Address modifiedaddress = createDomain(
				com.sugarify.core.domain.Address.class, 1);
		Response response = call(request("/users/" + user.getId()
				+ "/addresses/" + address.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedaddress)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		address.merge();
		assertTrue(compare(modifiedaddress, address, 1));
	}

	@Test
	public void deleteUserAddressTest() {
		com.sugarify.core.domain.User user = createDomain(com.sugarify.core.domain.User.class);
		user.persist();
		com.sugarify.core.domain.Address address = createDomain(com.sugarify.core.domain.Address.class);
		user.collection("addresses").add(address);
		user.persist();
		Response response = call(request("/users/" + user.getId()
				+ "/addresses/" + address.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/users/" + user.getId()
				+ "/addresses/" + address.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(address)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
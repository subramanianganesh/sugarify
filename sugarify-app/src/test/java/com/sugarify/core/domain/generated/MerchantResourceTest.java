package com.sugarify.core.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;

import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class MerchantResourceTest extends BaseJPAResourceTest {
	@Test
	public void listMerchantTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		Response response = call(request("/merchants/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.core.domain.Merchant.class).size(),
				(int) com.sugarify.core.domain.Merchant.count());
	}

	@Test
	public void readMerchantTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		Response response = call(request(
				"/merchants/" + merchant.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.core.domain.Merchant.class)
				.getId(), merchant.getId());
	}

	@Test
	public void createMerchantTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		Response response = call(request("/merchants/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(merchant)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		com.sugarify.core.domain.Merchant modifiedmerchant = createDomain(
				com.sugarify.core.domain.Merchant.class, 1);
		Response response = call(request(
				"/merchants/" + merchant.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedmerchant)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		merchant.merge();
		assertTrue(compare(modifiedmerchant, merchant, 1));
	}

	@Test
	public void deleteMerchantTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		Response response = call(request(
				"/merchants/" + merchant.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/merchants/" + merchant.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(merchant)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listMerchantLocationTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();

		com.sugarify.core.domain.MerchantLocation location = createDomain(com.sugarify.core.domain.MerchantLocation.class);
		merchant.collection("locations").add(location);
		merchant.persist();

		Response response = call(request(
				"/merchants/" + merchant.getId()
						+ "/locations/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserializeCollection(
						response.getContent(),
						java.util.List.class,
						com.sugarify.core.domain.MerchantLocation.class)
				.size(), merchant.getLocations().size());
	}

	@Test
	public void readMerchantLocationTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		com.sugarify.core.domain.MerchantLocation location = createDomain(com.sugarify.core.domain.MerchantLocation.class);
		merchant.collection("locations").add(location);
		merchant.persist();
		Response response = call(request(
				"/merchants/" + merchant.getId()
						+ "/locations/"
						+ location.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserialize(response.getContent(),
						com.sugarify.core.domain.MerchantLocation.class)
				.getId(), location.getId());
	}

	@Test
	public void createMerchantLocationTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		com.sugarify.core.domain.MerchantLocation location = createDomain(com.sugarify.core.domain.MerchantLocation.class);
		Response response = call(request(
				"/merchants/" + merchant.getId()
						+ "/locations/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(location)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantLocationTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		com.sugarify.core.domain.MerchantLocation location = createDomain(com.sugarify.core.domain.MerchantLocation.class);
		merchant.collection("locations").add(location);
		merchant.persist();
		com.sugarify.core.domain.MerchantLocation modifiedlocation = createDomain(
				com.sugarify.core.domain.MerchantLocation.class,
				1);
		Response response = call(request(
				"/merchants/" + merchant.getId()
						+ "/locations/"
						+ location.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedlocation)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		location.merge();
		assertTrue(compare(modifiedlocation, location, 1));
	}

	@Test
	public void deleteMerchantLocationTest() {
		com.sugarify.core.domain.Merchant merchant = createDomain(com.sugarify.core.domain.Merchant.class);
		merchant.persist();
		com.sugarify.core.domain.MerchantLocation location = createDomain(com.sugarify.core.domain.MerchantLocation.class);
		merchant.collection("locations").add(location);
		merchant.persist();
		Response response = call(request(
				"/merchants/" + merchant.getId()
						+ "/locations/"
						+ location.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/merchants/" + merchant.getId()
				+ "/locations/" + location.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(location)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
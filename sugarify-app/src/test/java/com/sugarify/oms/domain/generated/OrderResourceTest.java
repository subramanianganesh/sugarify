package com.sugarify.oms.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;
import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class OrderResourceTest extends BaseJPAResourceTest {
	@Test
	public void listOrderTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		Response response = call(request("/orders/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.Order.class).size(),
				(int) com.sugarify.oms.domain.Order.count());
	}

	@Test
	public void readOrderTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		Response response = call(request("/orders/" + order.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.Order.class).getId(),
				order.getId());
	}

	@Test
	public void createOrderTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		Response response = call(request("/orders/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(order)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.Order modifiedorder = createDomain(
				com.sugarify.oms.domain.Order.class, 1);
		Response response = call(request("/orders/" + order.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedorder)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		order.merge();
		assertTrue(compare(modifiedorder, order, 1));
	}

	@Test
	public void deleteOrderTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		Response response = call(request("/orders/" + order.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(order)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderAdjustment.class);
		order.collection("adjustments").add(adjustment);
		order.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/adjustments/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderAdjustment.class)
				.size(), order.getAdjustments().size());
	}

	@Test
	public void readOrderAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderAdjustment.class);
		order.collection("adjustments").add(adjustment);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/adjustments/" + adjustment.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderAdjustment.class)
				.getId(), adjustment.getId());
	}

	@Test
	public void createOrderAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderAdjustment.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/adjustments/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(adjustment)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderAdjustment.class);
		order.collection("adjustments").add(adjustment);
		order.persist();
		com.sugarify.oms.domain.OrderAdjustment modifiedadjustment = createDomain(
				com.sugarify.oms.domain.OrderAdjustment.class,
				1);
		Response response = call(request("/orders/" + order.getId()
				+ "/adjustments/" + adjustment.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedadjustment)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		adjustment.merge();
		assertTrue(compare(modifiedadjustment, adjustment, 1));
	}

	@Test
	public void deleteOrderAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderAdjustment.class);
		order.collection("adjustments").add(adjustment);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/adjustments/" + adjustment.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId()
				+ "/adjustments/" + adjustment.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(adjustment)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderNote note = createDomain(com.sugarify.oms.domain.OrderNote.class);
		order.collection("notes").add(note);
		order.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/notes/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderNote.class).size(),
				order.getNotes().size());
	}

	@Test
	public void readOrderNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderNote note = createDomain(com.sugarify.oms.domain.OrderNote.class);
		order.collection("notes").add(note);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/notes/" + note.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderNote.class)
				.getId(), note.getId());
	}

	@Test
	public void createOrderNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderNote note = createDomain(com.sugarify.oms.domain.OrderNote.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/notes/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(note)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderNote note = createDomain(com.sugarify.oms.domain.OrderNote.class);
		order.collection("notes").add(note);
		order.persist();
		com.sugarify.oms.domain.OrderNote modifiednote = createDomain(
				com.sugarify.oms.domain.OrderNote.class, 1);
		Response response = call(request("/orders/" + order.getId()
				+ "/notes/" + note.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiednote)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		note.merge();
		assertTrue(compare(modifiednote, note, 1));
	}

	@Test
	public void deleteOrderNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderNote note = createDomain(com.sugarify.oms.domain.OrderNote.class);
		order.collection("notes").add(note);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/notes/" + note.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId() + "/notes/"
				+ note.getId(), HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(note)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderRequest request = createDomain(com.sugarify.oms.domain.OrderRequest.class);
		order.collection("requests").add(request);
		order.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/requests/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderRequest.class)
				.size(), order.getRequests().size());
	}

	@Test
	public void readOrderRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderRequest request = createDomain(com.sugarify.oms.domain.OrderRequest.class);
		order.collection("requests").add(request);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/requests/" + request.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderRequest.class)
				.getId(), request.getId());
	}

	@Test
	public void createOrderRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderRequest request = createDomain(com.sugarify.oms.domain.OrderRequest.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/requests/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(request)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderRequest request = createDomain(com.sugarify.oms.domain.OrderRequest.class);
		order.collection("requests").add(request);
		order.persist();
		com.sugarify.oms.domain.OrderRequest modifiedrequest = createDomain(
				com.sugarify.oms.domain.OrderRequest.class, 1);
		Response response = call(request("/orders/" + order.getId()
				+ "/requests/" + request.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedrequest)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		request.merge();
		assertTrue(compare(modifiedrequest, request, 1));
	}

	@Test
	public void deleteOrderRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderRequest request = createDomain(com.sugarify.oms.domain.OrderRequest.class);
		order.collection("requests").add(request);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/requests/" + request.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId()
				+ "/requests/" + request.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(request)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderItemTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/items/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderItem.class).size(),
				order.getItems().size());
	}

	@Test
	public void readOrderItemTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderItem.class)
				.getId(), item.getId());
	}

	@Test
	public void createOrderItemTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(item)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderItemTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItem modifieditem = createDomain(
				com.sugarify.oms.domain.OrderItem.class, 1);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifieditem)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		item.merge();
		assertTrue(compare(modifieditem, item, 1));
	}

	@Test
	public void deleteOrderItemTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId() + "/items/"
				+ item.getId(), HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(item)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderItemAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();

		com.sugarify.oms.domain.OrderItemAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderItemAdjustment.class);
		item.collection("adjustments").add(adjustment);
		item.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/adjustments/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserializeCollection(
						response.getContent(),
						java.util.List.class,
						com.sugarify.oms.domain.OrderItemAdjustment.class)
				.size(), item.getAdjustments().size());
	}

	@Test
	public void readOrderItemAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderItemAdjustment.class);
		item.collection("adjustments").add(adjustment);
		item.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/adjustments/"
				+ adjustment.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserialize(response.getContent(),
						com.sugarify.oms.domain.OrderItemAdjustment.class)
				.getId(), adjustment.getId());
	}

	@Test
	public void createOrderItemAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderItemAdjustment.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/adjustments/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(adjustment)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderItemAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderItemAdjustment.class);
		item.collection("adjustments").add(adjustment);
		item.persist();
		com.sugarify.oms.domain.OrderItemAdjustment modifiedadjustment = createDomain(
				com.sugarify.oms.domain.OrderItemAdjustment.class,
				1);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/adjustments/"
				+ adjustment.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedadjustment)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		adjustment.merge();
		assertTrue(compare(modifiedadjustment, adjustment, 1));
	}

	@Test
	public void deleteOrderItemAdjustmentTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemAdjustment adjustment = createDomain(com.sugarify.oms.domain.OrderItemAdjustment.class);
		item.collection("adjustments").add(adjustment);
		item.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/adjustments/"
				+ adjustment.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/orders/" + order.getId() + "/items/"
						+ item.getId()
						+ "/adjustments/"
						+ adjustment.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(adjustment)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderItemNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();

		com.sugarify.oms.domain.OrderItemNote note = createDomain(com.sugarify.oms.domain.OrderItemNote.class);
		item.collection("notes").add(note);
		item.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/notes/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderItemNote.class)
				.size(), item.getNotes().size());
	}

	@Test
	public void readOrderItemNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemNote note = createDomain(com.sugarify.oms.domain.OrderItemNote.class);
		item.collection("notes").add(note);
		item.persist();
		Response response = call(request(
				"/orders/" + order.getId() + "/items/"
						+ item.getId() + "/notes/"
						+ note.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderItemNote.class)
				.getId(), note.getId());
	}

	@Test
	public void createOrderItemNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemNote note = createDomain(com.sugarify.oms.domain.OrderItemNote.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/notes/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(note)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderItemNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemNote note = createDomain(com.sugarify.oms.domain.OrderItemNote.class);
		item.collection("notes").add(note);
		item.persist();
		com.sugarify.oms.domain.OrderItemNote modifiednote = createDomain(
				com.sugarify.oms.domain.OrderItemNote.class, 1);
		Response response = call(request(
				"/orders/" + order.getId() + "/items/"
						+ item.getId() + "/notes/"
						+ note.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiednote)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		note.merge();
		assertTrue(compare(modifiednote, note, 1));
	}

	@Test
	public void deleteOrderItemNoteTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemNote note = createDomain(com.sugarify.oms.domain.OrderItemNote.class);
		item.collection("notes").add(note);
		item.persist();
		Response response = call(request(
				"/orders/" + order.getId() + "/items/"
						+ item.getId() + "/notes/"
						+ note.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/orders/" + order.getId() + "/items/"
				+ item.getId() + "/notes/" + note.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(note)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOrderItemRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();

		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();

		com.sugarify.oms.domain.OrderItemRequest request = createDomain(com.sugarify.oms.domain.OrderItemRequest.class);
		item.collection("requests").add(request);
		item.persist();

		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/requests/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.oms.domain.OrderItemRequest.class)
				.size(), item.getRequests().size());
	}

	@Test
	public void readOrderItemRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemRequest request = createDomain(com.sugarify.oms.domain.OrderItemRequest.class);
		item.collection("requests").add(request);
		item.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/requests/"
				+ request.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.oms.domain.OrderItemRequest.class)
				.getId(), request.getId());
	}

	@Test
	public void createOrderItemRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemRequest request = createDomain(com.sugarify.oms.domain.OrderItemRequest.class);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/requests/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(request)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOrderItemRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemRequest request = createDomain(com.sugarify.oms.domain.OrderItemRequest.class);
		item.collection("requests").add(request);
		item.persist();
		com.sugarify.oms.domain.OrderItemRequest modifiedrequest = createDomain(
				com.sugarify.oms.domain.OrderItemRequest.class,
				1);
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/requests/"
				+ request.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedrequest)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		request.merge();
		assertTrue(compare(modifiedrequest, request, 1));
	}

	@Test
	public void deleteOrderItemRequestTest() {
		com.sugarify.oms.domain.Order order = createDomain(com.sugarify.oms.domain.Order.class);
		order.persist();
		com.sugarify.oms.domain.OrderItem item = createDomain(com.sugarify.oms.domain.OrderItem.class);
		order.collection("items").add(item);
		order.persist();
		com.sugarify.oms.domain.OrderItemRequest request = createDomain(com.sugarify.oms.domain.OrderItemRequest.class);
		item.collection("requests").add(request);
		item.persist();
		Response response = call(request("/orders/" + order.getId()
				+ "/items/" + item.getId() + "/requests/"
				+ request.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/orders/" + order.getId() + "/items/"
						+ item.getId() + "/requests/"
						+ request.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(request)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
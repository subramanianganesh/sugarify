package com.sugarify.cms.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;

import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class OptionTypeResourceTest extends BaseJPAResourceTest {
	@Test
	public void listOptionTypeTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		Response response = call(request("/option_types/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.OptionType.class)
				.size(),
				(int) com.sugarify.cms.domain.OptionType
						.count());
	}

	@Test
	public void readOptionTypeTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		Response response = call(request(
				"/option_types/" + optionType.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.OptionType.class)
				.getId(), optionType.getId());
	}

	@Test
	public void createOptionTypeTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		Response response = call(request("/option_types/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(optionType)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOptionTypeTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		com.sugarify.cms.domain.OptionType modifiedoptionType = createDomain(
				com.sugarify.cms.domain.OptionType.class, 1);
		Response response = call(request(
				"/option_types/" + optionType.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedoptionType)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		optionType.merge();
		assertTrue(compare(modifiedoptionType, optionType, 1));
	}

	@Test
	public void deleteOptionTypeTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		Response response = call(request(
				"/option_types/" + optionType.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/option_types/" + optionType.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(optionType)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listOptionTypeValueTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();

		com.sugarify.cms.domain.OptionValue value = createDomain(com.sugarify.cms.domain.OptionValue.class);
		optionType.collection("values").add(value);
		optionType.persist();

		Response response = call(request(
				"/option_types/" + optionType.getId()
						+ "/values/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.OptionValue.class)
				.size(), optionType.getValues().size());
	}

	@Test
	public void readOptionTypeValueTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		com.sugarify.cms.domain.OptionValue value = createDomain(com.sugarify.cms.domain.OptionValue.class);
		optionType.collection("values").add(value);
		optionType.persist();
		Response response = call(request(
				"/option_types/" + optionType.getId()
						+ "/values/" + value.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.OptionValue.class)
				.getId(), value.getId());
	}

	@Test
	public void createOptionTypeValueTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		com.sugarify.cms.domain.OptionValue value = createDomain(com.sugarify.cms.domain.OptionValue.class);
		Response response = call(request(
				"/option_types/" + optionType.getId()
						+ "/values/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(value)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateOptionTypeValueTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		com.sugarify.cms.domain.OptionValue value = createDomain(com.sugarify.cms.domain.OptionValue.class);
		optionType.collection("values").add(value);
		optionType.persist();
		com.sugarify.cms.domain.OptionValue modifiedvalue = createDomain(
				com.sugarify.cms.domain.OptionValue.class, 1);
		Response response = call(request(
				"/option_types/" + optionType.getId()
						+ "/values/" + value.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedvalue)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		value.merge();
		assertTrue(compare(modifiedvalue, value, 1));
	}

	@Test
	public void deleteOptionTypeValueTest() {
		com.sugarify.cms.domain.OptionType optionType = createDomain(com.sugarify.cms.domain.OptionType.class);
		optionType.persist();
		com.sugarify.cms.domain.OptionValue value = createDomain(com.sugarify.cms.domain.OptionValue.class);
		optionType.collection("values").add(value);
		optionType.persist();
		Response response = call(request(
				"/option_types/" + optionType.getId()
						+ "/values/" + value.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/option_types/" + optionType.getId()
				+ "/values/" + value.getId(), HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(value)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
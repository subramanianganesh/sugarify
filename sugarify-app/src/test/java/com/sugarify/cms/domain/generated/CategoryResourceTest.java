package com.sugarify.cms.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;
import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class CategoryResourceTest extends BaseJPAResourceTest {
	@Test
	public void listCategoryTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		Response response = call(request("/categories/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.Category.class).size(),
				(int) com.sugarify.cms.domain.Category.count());
	}

	@Test
	public void readCategoryTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		Response response = call(request(
				"/categories/" + category.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.Category.class).getId(),
				category.getId());
	}

	@Test
	public void createCategoryTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		Response response = call(request("/categories/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(category)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateCategoryTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		com.sugarify.cms.domain.Category modifiedcategory = createDomain(
				com.sugarify.cms.domain.Category.class, 1);
		Response response = call(request(
				"/categories/" + category.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedcategory)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		category.merge();
		assertTrue(compare(modifiedcategory, category, 1));
	}

	@Test
	public void deleteCategoryTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		Response response = call(request(
				"/categories/" + category.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/categories/" + category.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(category)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listCategoryChildTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();

		com.sugarify.cms.domain.Category child = createDomain(com.sugarify.cms.domain.Category.class);
		category.collection("children").add(child);
		category.persist();

		Response response = call(request(
				"/categories/" + category.getId()
						+ "/children/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.Category.class).size(),
				category.getChildren().size());
	}

	@Test
	public void readCategoryChildTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		com.sugarify.cms.domain.Category child = createDomain(com.sugarify.cms.domain.Category.class);
		category.collection("children").add(child);
		category.persist();
		Response response = call(request(
				"/categories/" + category.getId()
						+ "/children/" + child.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.Category.class).getId(),
				child.getId());
	}

	@Test
	public void createCategoryChildTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		com.sugarify.cms.domain.Category child = createDomain(com.sugarify.cms.domain.Category.class);
		Response response = call(request(
				"/categories/" + category.getId()
						+ "/children/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(child)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateCategoryChildTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		com.sugarify.cms.domain.Category child = createDomain(com.sugarify.cms.domain.Category.class);
		category.collection("children").add(child);
		category.persist();
		com.sugarify.cms.domain.Category modifiedchild = createDomain(
				com.sugarify.cms.domain.Category.class, 1);
		Response response = call(request(
				"/categories/" + category.getId()
						+ "/children/" + child.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedchild)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		child.merge();
		assertTrue(compare(modifiedchild, child, 1));
	}

	@Test
	public void deleteCategoryChildTest() {
		com.sugarify.cms.domain.Category category = createDomain(com.sugarify.cms.domain.Category.class);
		category.persist();
		com.sugarify.cms.domain.Category child = createDomain(com.sugarify.cms.domain.Category.class);
		category.collection("children").add(child);
		category.persist();
		Response response = call(request(
				"/categories/" + category.getId()
						+ "/children/" + child.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/categories/" + category.getId()
				+ "/children/" + child.getId(), HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(child)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}
	
	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
package com.sugarify.cms.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;

import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class MerchantProductResourceTest extends BaseJPAResourceTest {
	@Test
	public void listMerchantProductTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		Response response = call(request("/merchant_products/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.MerchantProduct.class)
				.size(),
				(int) com.sugarify.cms.domain.MerchantProduct
						.count());
	}

	@Test
	public void readMerchantProductTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.MerchantProduct.class)
				.getId(), merchantProduct.getId());
	}

	@Test
	public void createMerchantProductTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		Response response = call(request("/merchant_products/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(merchantProduct)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantProductTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProduct modifiedmerchantProduct = createDomain(
				com.sugarify.cms.domain.MerchantProduct.class,
				1);
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedmerchantProduct)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		merchantProduct.merge();
		assertTrue(compare(modifiedmerchantProduct, merchantProduct, 1));
	}

	@Test
	public void deleteMerchantProductTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/merchant_products/" + merchantProduct.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(merchantProduct)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listMerchantProductVariantTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();

		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();

		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserializeCollection(
						response.getContent(),
						java.util.List.class,
						com.sugarify.cms.domain.MerchantProductVariant.class)
				.size(), merchantProduct.getVariants().size());
	}

	@Test
	public void readMerchantProductVariantTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserialize(response.getContent(),
						com.sugarify.cms.domain.MerchantProductVariant.class)
				.getId(), variant.getId());
	}

	@Test
	public void createMerchantProductVariantTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(variant)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantProductVariantTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant modifiedvariant = createDomain(
				com.sugarify.cms.domain.MerchantProductVariant.class,
				1);
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedvariant)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		variant.merge();
		assertTrue(compare(modifiedvariant, variant, 1));
	}

	@Test
	public void deleteMerchantProductVariantTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(variant)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listMerchantProductVariantOptionTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();

		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();

		com.sugarify.cms.domain.MerchantProductVariantOption option = createDomain(com.sugarify.cms.domain.MerchantProductVariantOption.class);
		variant.collection("options").add(option);
		variant.persist();

		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId() + "/options/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserializeCollection(
						response.getContent(),
						java.util.List.class,
						com.sugarify.cms.domain.MerchantProductVariantOption.class)
				.size(), variant.getOptions().size());
	}

	@Test
	public void readMerchantProductVariantOptionTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariantOption option = createDomain(com.sugarify.cms.domain.MerchantProductVariantOption.class);
		variant.collection("options").add(option);
		variant.persist();
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId() + "/options/"
						+ option.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer
				.deserialize(response.getContent(),
						com.sugarify.cms.domain.MerchantProductVariantOption.class)
				.getId(), option.getId());
	}

	@Test
	public void createMerchantProductVariantOptionTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariantOption option = createDomain(com.sugarify.cms.domain.MerchantProductVariantOption.class);
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId() + "/options/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(option)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantProductVariantOptionTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariantOption option = createDomain(com.sugarify.cms.domain.MerchantProductVariantOption.class);
		variant.collection("options").add(option);
		variant.persist();
		com.sugarify.cms.domain.MerchantProductVariantOption modifiedoption = createDomain(
				com.sugarify.cms.domain.MerchantProductVariantOption.class,
				1);
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId() + "/options/"
						+ option.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedoption)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		option.merge();
		assertTrue(compare(modifiedoption, option, 1));
	}

	@Test
	public void deleteMerchantProductVariantOptionTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariantOption option = createDomain(com.sugarify.cms.domain.MerchantProductVariantOption.class);
		variant.collection("options").add(option);
		variant.persist();
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId() + "/options/"
						+ option.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId() + "/options/"
						+ option.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(option)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listMerchantProductVariantInventoryTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();

		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();

		com.sugarify.cms.domain.Inventory inventory = createDomain(com.sugarify.cms.domain.Inventory.class);
		variant.collection("inventories").add(inventory);
		variant.persist();

		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId() + "/inventories/",
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.cms.domain.Inventory.class).size(),
				variant.getInventories().size());
	}

	@Test
	public void readMerchantProductVariantInventoryTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.Inventory inventory = createDomain(com.sugarify.cms.domain.Inventory.class);
		variant.collection("inventories").add(inventory);
		variant.persist();
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId()
						+ "/inventories/"
						+ inventory.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.cms.domain.Inventory.class)
				.getId(), inventory.getId());
	}

	@Test
	public void createMerchantProductVariantInventoryTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.Inventory inventory = createDomain(com.sugarify.cms.domain.Inventory.class);
		Response response = call(request("/merchant_products/"
				+ merchantProduct.getId() + "/variants/"
				+ variant.getId() + "/inventories/",
				HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(inventory)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateMerchantProductVariantInventoryTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.Inventory inventory = createDomain(com.sugarify.cms.domain.Inventory.class);
		variant.collection("inventories").add(inventory);
		variant.persist();
		com.sugarify.cms.domain.Inventory modifiedinventory = createDomain(
				com.sugarify.cms.domain.Inventory.class, 1);
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId()
						+ "/inventories/"
						+ inventory.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedinventory)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		inventory.merge();
		assertTrue(compare(modifiedinventory, inventory, 1));
	}

	@Test
	public void deleteMerchantProductVariantInventoryTest() {
		com.sugarify.cms.domain.MerchantProduct merchantProduct = createDomain(com.sugarify.cms.domain.MerchantProduct.class);
		merchantProduct.persist();
		com.sugarify.cms.domain.MerchantProductVariant variant = createDomain(com.sugarify.cms.domain.MerchantProductVariant.class);
		merchantProduct.collection("variants").add(variant);
		merchantProduct.persist();
		com.sugarify.cms.domain.Inventory inventory = createDomain(com.sugarify.cms.domain.Inventory.class);
		variant.collection("inventories").add(inventory);
		variant.persist();
		Response response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId()
						+ "/inventories/"
						+ inventory.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request(
				"/merchant_products/" + merchantProduct.getId()
						+ "/variants/"
						+ variant.getId()
						+ "/inventories/"
						+ inventory.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(inventory)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
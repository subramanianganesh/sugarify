package com.sugarify.checkout.domain.generated;

import org.minnal.core.serializer.Serializer;
import org.minnal.core.resource.BaseJPAResourceTest;
import org.testng.annotations.Test;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.minnal.core.Response;
import static org.testng.Assert.*;

/**
 * This is an auto generated test class by minnal-generator
 */
public class CartResourceTest extends BaseJPAResourceTest {
	@Test
	public void listCartTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		Response response = call(request("/carts/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.checkout.domain.Cart.class).size(),
				(int) com.sugarify.checkout.domain.Cart.count());
	}

	@Test
	public void readCartTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		Response response = call(request("/carts/" + cart.getId(),
				HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.checkout.domain.Cart.class)
				.getId(), cart.getId());
	}

	@Test
	public void createCartTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		Response response = call(request("/carts/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(cart)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateCartTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		com.sugarify.checkout.domain.Cart modifiedcart = createDomain(
				com.sugarify.checkout.domain.Cart.class, 1);
		Response response = call(request("/carts/" + cart.getId(),
				HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifiedcart)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		cart.merge();
		assertTrue(compare(modifiedcart, cart, 1));
	}

	@Test
	public void deleteCartTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		Response response = call(request("/carts/" + cart.getId(),
				HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/carts/" + cart.getId(),
				HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(cart)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Test
	public void listCartItemTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();

		com.sugarify.checkout.domain.CartItem item = createDomain(com.sugarify.checkout.domain.CartItem.class);
		cart.collection("items").add(item);
		cart.persist();

		Response response = call(request("/carts/" + cart.getId()
				+ "/items/", HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserializeCollection(
				response.getContent(), java.util.List.class,
				com.sugarify.checkout.domain.CartItem.class)
				.size(), cart.getItems().size());
	}

	@Test
	public void readCartItemTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		com.sugarify.checkout.domain.CartItem item = createDomain(com.sugarify.checkout.domain.CartItem.class);
		cart.collection("items").add(item);
		cart.persist();
		Response response = call(request("/carts/" + cart.getId()
				+ "/items/" + item.getId(), HttpMethod.GET));
		assertEquals(response.getStatus(), HttpResponseStatus.OK);
		assertEquals(serializer.deserialize(response.getContent(),
				com.sugarify.checkout.domain.CartItem.class)
				.getId(), item.getId());
	}

	@Test
	public void createCartItemTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		com.sugarify.checkout.domain.CartItem item = createDomain(com.sugarify.checkout.domain.CartItem.class);
		Response response = call(request("/carts/" + cart.getId()
				+ "/items/", HttpMethod.POST,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(item)));
		assertEquals(response.getStatus(), HttpResponseStatus.CREATED);
	}

	@Test
	public void updateCartItemTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		com.sugarify.checkout.domain.CartItem item = createDomain(com.sugarify.checkout.domain.CartItem.class);
		cart.collection("items").add(item);
		cart.persist();
		com.sugarify.checkout.domain.CartItem modifieditem = createDomain(
				com.sugarify.checkout.domain.CartItem.class, 1);
		Response response = call(request("/carts/" + cart.getId()
				+ "/items/" + item.getId(), HttpMethod.PUT,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(modifieditem)));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		item.merge();
		assertTrue(compare(modifieditem, item, 1));
	}

	@Test
	public void deleteCartItemTest() {
		com.sugarify.checkout.domain.Cart cart = createDomain(com.sugarify.checkout.domain.Cart.class);
		cart.persist();
		com.sugarify.checkout.domain.CartItem item = createDomain(com.sugarify.checkout.domain.CartItem.class);
		cart.collection("items").add(item);
		cart.persist();
		Response response = call(request("/carts/" + cart.getId()
				+ "/items/" + item.getId(), HttpMethod.DELETE));
		assertEquals(response.getStatus(),
				HttpResponseStatus.NO_CONTENT);
		response = call(request("/carts/" + cart.getId() + "/items/"
				+ item.getId(), HttpMethod.GET,
				Serializer.DEFAULT_JSON_SERIALIZER
						.serialize(item)));
		assertEquals(response.getStatus(), HttpResponseStatus.NOT_FOUND);
	}

	@Override
	protected boolean disableForeignKeyChecks() {
		return false;
	}
}
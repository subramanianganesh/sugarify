/**
 * 
 */
package com.sugarify.app;

import org.activejpa.util.EnumConverter;
import org.apache.commons.beanutils.ConvertUtils;
import org.minnal.core.Application;
import org.minnal.jpa.JPAPlugin;
import org.minnal.jpa.OpenSessionInViewFilter;
import org.minnal.migrations.plugin.MigrationsPlugin;
import org.minnal.validation.ValidationPlugin;

import com.sugarify.checkout.domain.Cart;
import com.sugarify.core.domain.Address;

/**
 * @author ganeshs
 * 
 */
public class SugarifyApplication extends Application<SugarifyConfiguration> {

	/**
	 * Default constructor
	 */
	public SugarifyApplication() {
		ConvertUtils.register(EnumConverter.instance, Address.Type.class);
		ConvertUtils.register(EnumConverter.instance, Cart.Status.class);
	}

	@Override
	protected void registerPlugins() {
		registerPlugin(new MigrationsPlugin());
		registerPlugin(new JPAPlugin());
		registerPlugin(new ValidationPlugin());
	}

	@Override
	protected void addFilters() {
		addFilter(new OpenSessionInViewFilter(getConfiguration().getDatabaseConfiguration()));
	}

	@Override
	protected void defineRoutes() {

	}

	@Override
	protected void defineResources() {

	}
}

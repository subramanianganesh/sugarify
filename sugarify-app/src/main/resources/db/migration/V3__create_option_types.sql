create table option_types (
  id bigint(20) not null primary key auto_increment,
  name varchar(50) not null,
  display_name varchar(100),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table option_values (
  id bigint(20) not null primary key auto_increment,
  type_id bigint(20) not null,
  value varchar(50) not null,
  display_value varchar(100) not null,
  position int(10) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);
create table categories (
  id bigint(20) not null primary key auto_increment,
  name varchar(255) not null,
  parent_id bigint(20),
  description text,
  image_url varchar(255) not null,
  enabled tinyint(1) not null default 1,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table category_attributes (
  id bigint(20) not null primary key auto_increment,
  category_id bigint(20) not null,
  name varchar(50) not null,
  value text,
  updated_at timestamp not null default current_timestamp on update current_timestamp
);

create table products (
  id bigint(20) not null primary key auto_increment,
  category_id bigint(20) not null,
  name varchar(255) not null,
  description text,
  meta_description text,
  meta_keywords text,
  enabled tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table product_attributes (
  id bigint(20) not null primary key auto_increment,
  product_id bigint(20) not null,
  name varchar(50) not null,
  value text,
  updated_at timestamp not null default current_timestamp on update current_timestamp
);
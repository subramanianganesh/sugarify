create table carts (
  id bigint(20) not null primary key auto_increment,
  user_id bigint(20),
  status varchar(20) not null,
  deleted tinyint(1) not null default 0,
  locked tinyint(1) not null default 0,
  lock_expiry_time timestamp(3),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table cart_items (
  id bigint(20) not null primary key auto_increment,
  cart_id bigint(20) not null,
  merchant_product_id bigint(20) not null,
  variant_id bigint(20) not null,
  quantity int(10) not null,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);
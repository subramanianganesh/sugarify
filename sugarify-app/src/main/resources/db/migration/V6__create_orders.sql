create table orders (
  id bigint(20) not null primary key auto_increment,
  user_id bigint(20),
  external_id varchar(20),
  status varchar(20) not null,
  order_date timestamp not null,
  shipping_address_id bigint(20),
  billing_address_id bigint(20),
  total double not null default 0,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_items (
  id bigint(20) not null primary key auto_increment,
  order_id bigint(20) not null,
  merchant_id bigint(20) not null,
  merchant_product_id bigint(20) not null,
  variant_id bigint(20) not null,
  status varchar(20) not null,
  quantity int(10) not null,
  price double not null,
  title varchar(255) not null,
  description text,
  total double not null default 0,
  order_date timestamp not null,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_adjustments (
  id bigint(20) not null primary key auto_increment,
  order_id bigint(20) not null,
  type varchar(20) not null,
  value double not null default 0,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_item_adjustments (
  id bigint(20) not null primary key auto_increment,
  order_item_id bigint(20) not null,
  type varchar(20) not null,
  value double not null default 0,
  deleted tinyint(1) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_attributes (
  id bigint(20) not null primary key auto_increment,
  order_id bigint(20) not null,
  name varchar(50) not null,
  value text,
  updated_at timestamp not null default current_timestamp on update current_timestamp
);

create table order_item_attributes (
  id bigint(20) not null primary key auto_increment,
  order_item_id bigint(20) not null,
  name varchar(50) not null,
  value text,
  updated_at timestamp not null default current_timestamp on update current_timestamp
);

create table order_notes (
  id bigint(20) not null primary key auto_increment,
  order_id bigint(20) not null,
  note text not null,
  created_by varchar(50),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_item_notes (
  id bigint(20) not null primary key auto_increment,
  order_item_id bigint(20) not null,
  note text not null,
  created_by varchar(50),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_requests(
  id bigint(20) not null primary key auto_increment,
  order_id bigint(20) not null,
  type varchar(20) not null,
  reason varchar(255),
  created_by varchar(50),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table order_item_requests(
  id bigint(20) not null primary key auto_increment,
  order_item_id bigint(20) not null,
  type varchar(20) not null,
  quantity int(10) not null,
  successful_quantity int(10) not null,
  reason varchar(255),
  created_by varchar(50),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);
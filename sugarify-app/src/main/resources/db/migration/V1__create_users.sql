create table users (
  id bigint(20) not null primary key auto_increment,
  first_name varchar(255) not null,
  last_name varchar(255),
  email varchar(255) not null,
  password varchar(255) not null,
  password_salt varchar(128) not null,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table addresses (
  id bigint(20) not null primary key auto_increment,
  user_id bigint(20) not null,
  type varchar(10) not null,
  address_line1 varchar(255),
  address_line2 varchar(255),
  city varchar(50) not null,
  state varchar(50) not null,
  country varchar(50) not null,
  pincode varchar(20) not null,
  phone1 varchar(20),
  phone2 varchar(20),
  enabled tinyint(1) not null default 1,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);
create table merchants (
  id bigint(20) not null primary key auto_increment,
  name varchar(255) not null,
  display_name varchar(255),
  code varchar(50) not null,
  description text,
  website varchar(255),
  email varchar(255),
  phone1 varchar(20),
  phone2 varchar(20),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table merchant_attributes (
  id bigint(20) not null primary key auto_increment,
  merchant_id bigint(20) not null,
  name varchar(50) not null,
  value text,
  updated_at timestamp not null default current_timestamp on update current_timestamp
);

create table merchant_locations (
  id bigint(20) not null primary key auto_increment,
  merchant_id bigint(20) not null,
  name varchar(50),
  manager_name varchar(50),
  address_line1 varchar(255),
  address_line2 varchar(255),
  city varchar(50) not null,
  state varchar(50) not null,
  country varchar(50) not null,
  pincode varchar(20) not null,
  phone1 varchar(20),
  phone2 varchar(20),
  active tinyint(1) not null default 1,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table merchant_products (
  id bigint(20) not null primary key auto_increment,
  merchant_id bigint(20) not null,
  product_id bigint(20) not null,
  cost_price double,
  sale_price double,
  meta_description text,
  meta_keywords text,
  enabled tinyint(1) not null default 1,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table merchant_product_variants (
  id bigint(20) not null primary key auto_increment,
  merchant_product_id bigint(20) not null,
  sku varchar(50) not null,
  cost_price double,
  sale_price double not null,
  position int(10) not null default 0,
  track_inventory tinyint(1) not null default 1,
  enabled tinyint(1) not null default 1,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table merchant_product_variant_options (
  id bigint(20) not null primary key auto_increment,
  variant_id bigint(20) not null,
  type_id bigint(20) not null,
  value_id bigint(20),
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);

create table inventories (
  id bigint(20) not null primary key auto_increment,
  merchant_id bigint(20) not null,
  variant_id bigint(20) not null,
  location_id bigint(20) not null,
  quantity int(10) not null default 0,
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null default '0000-00-00 00:00:00'
);